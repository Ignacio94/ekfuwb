#include "EKFUWB.h"


void EKFUWB::init(void){
    entro=0;
    X_k.setZero(9,1);
    P_k.setZero(9,9);
    X_k1.setZero(9,1);
    P_k1.setZero(9,9);
    H_k.setZero(6,9);
    R_k.setZero(6,6);
    Q_k.setZero(9,9);
    U_k.setZero(9,9);
    Inverse_k.setZero(6,6);
    measurement.setZero(6,1);
    rangosX.setZero(6,1);
    difrangos.setZero(6,1);
    K_k.setZero(9,6);
    tags(0,0)=2.15;
    tags(1,0)=0;
    tags(2,0)=1;
    tags(0,1)=1.82;
    tags(1,1)=2.18;
    tags(2,1)=1.25;
    tags(0,2)=2.2;
    tags(1,2)=1.6;
    tags(2,2)=1;
    tags(0,3)=1.73;
    tags(1,3)=1.22;
    tags(2,3)=0.70;
    tags(0,4)=-0.98;
    tags(1,4)=1.43;
    tags(2,4)=0.57;
    tags(0,5)=2.3;
    tags(1,5)=0.82;
    tags(2,5)=1.60;
    X_k(0,0)=0;
    X_k(1,0)=0;
    X_k(2,0)=1;
    var_movi=0.005;
    update_rate=50.0;
    delta_t=1/update_rate;
    var_rango=2.5;
    Q_k(0,0)= 0;
    Q_k(1,1)= 0;
    Q_k(2,2)= 0;
    Q_k(3,3)= var_movi*pow(update_rate,2);
    Q_k(4,4)= var_movi*pow(update_rate,2);
    Q_k(5,5)= var_movi*pow(update_rate,2);
    U_k(0,0)= 1;
    U_k(1,1)= 1;
    U_k(2,2)= 1;
    U_k(3,3)= 1;
    U_k(4,4)= 1;
    U_k(5,5)= 1;
    U_k(0,3)= delta_t;
    U_k(1,4)= delta_t;
    U_k(2,5)= delta_t;

    //DroneModule::open(nh_);

    noUWB=100000;


}
void EKFUWB::run(void){
    X_k1=U_k*X_k;
    P_k1=U_k*P_k*U_k.transpose()+Q_k;

    H_k.setZero(6,9);
    R_k.setZero(6,6);
    difrangos.setZero(6,1);
    rangosX.setZero(6,1);
    tagstoupdate.setZero(3,6);
    measuretoupdate.setZero(1,6);
    Inverse_k.setZero(6,6);

    int j=0;
//    std::cout << "The measure "<< measurement<< std::endl;

    for (int i =0; i<measurement.rows();i++){
        if (measurement(i,0)!=noUWB && entro==1){
            measuretoupdate(0,j)=measurement(i,0)/1000.0;
            measurement(i,0)=noUWB;
            tagstoupdate.col(j)=tags.col(i);
            j=j+1;
        }
    }
    //cout<<j<<endl;
    if (j>0){
        measuretoupdate.conservativeResize((size_t) 1, (size_t) j);
        tagstoupdate.conservativeResize((size_t) 3,(size_t) j);
        R_k.conservativeResize((size_t) j,(size_t) j);
        rangosX.conservativeResize((size_t) j,(size_t) 1);
        H_k.conservativeResize((size_t) j,(size_t) 9);
        difrangos.conservativeResize((size_t) j,(size_t) 1);
        Inverse_k.conservativeResize((size_t) j,(size_t) j);
//        std::cout << "The measure "<< measuretoupdate<< std::endl;
//        std::cout << "The matrix tagstoupdate "<< tagstoupdate << std::endl;

        for (int m=0;m<j;m++){
            R_k(m,m)= var_rango;
            rangosX(m,0)=sqrt(pow(X_k1(0,0)-tagstoupdate(0,m),2)+pow(X_k1(1,0)-tagstoupdate(1,m),2)+pow(X_k1(2,0)-tagstoupdate(2,m),2));

            H_k(m,0)=-(tagstoupdate(0,m)-X_k1(0,0))/rangosX(m,0);
            H_k(m,1)=-(tagstoupdate(1,m)-X_k1(1,0))/rangosX(m,0);
            H_k(m,2)=-(tagstoupdate(2,m)-X_k1(2,0))/rangosX(m,0);
            difrangos(m,0)=measuretoupdate(0,m)-rangosX(m,0);

        }
//        std::cout << "The matrix X "<< X_k1<<  std::endl;
//        std::cout << "The matrix rangos "<< rangosX<<  std::endl;

//        std::cout << "The matrix R "<< R_k<<  std::endl;
//        std::cout << "The matrix P "<< P_k1<<  std::endl;
//        std::cout << "The matrix H "<< H_k<<  std::endl;

        Inverse_k=H_k*P_k1*H_k.transpose()*R_k;
//        std::cout << "The matrix Inverse "<< Inverse_k<<  std::endl;
        K_k=P_k1*H_k.transpose()*Inverse_k.inverse();
//        std::cout << "The matrix K "<< K_k<<  std::endl;
//        std::cout << "The matrix dif "<< difrangos<<  std::endl;
        X_k=X_k1+K_k*difrangos;
        P_k=(I_k.setIdentity(9,9)-K_k*H_k)*P_k1;
    }
    else{
        X_k=X_k1;
        P_k=P_k1;
    }
    pose.position.x=X_k(0,0);
    pose.position.y=X_k(1,0);
    pose.position.z=X_k(2,0);
    kalman_pub.publish(pose);

}
bool EKFUWB::startVal()
{

    
    //End
    //return DroneModule::startVal();
}

EKFUWB::EKFUWB(void)
{
    UWB_sub_ = nh_.subscribe("/decawave_range",1,&EKFUWB::UWBCb, this);
    kalman_pub = nh_.advertise<geometry_msgs::Pose>("/pose",1);


}
bool EKFUWB::stopVal()
{
    //Do stuff

    //return DroneModule::stopVal();
}

EKFUWB::~EKFUWB(void)
{

}
void EKFUWB::UWBCb(const droneMsgsROS::DecaWaveMsg& msg){
    measurement(0,0)=msg.T0;
    measurement(1,0)=msg.T1;
    measurement(2,0)=msg.T2;
    measurement(3,0)=msg.T3;
    measurement(4,0)=msg.T4;
    measurement(5,0)=msg.T5;
    entro=1;
}
