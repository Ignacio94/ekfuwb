#include "EKFUWB.h"
int main(int argc, char** argv)
{
    ros::init(argc, argv, "EKFUWB_node");
    EKFUWB ic;
    ic.init();
    ros::Rate r(50);
    while(ros::ok()){
        ros::spinOnce();
        ic.run();
        r.sleep();
    }
}

