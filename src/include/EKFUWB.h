#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/video/tracking.hpp>
#include <ros/ros.h>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <geometry_msgs/Pose.h>
#include <droneMsgsROS/DecaWaveMsg.h>


using namespace cv;
using namespace std;

Eigen::MatrixXd X_k(9,1), P_k(9,9),X_k1(9,1), P_k1(9,9) ;
Eigen::MatrixXd H_k(6,8), R_k(6,6);
Eigen::MatrixXd Q_k(9,9),  U_k(9,9);
Eigen::MatrixXd measurement(6,1);
Eigen::MatrixXd measuretoupdate(1,6);
Eigen::MatrixXd tags(3,6);
Eigen::MatrixXd tagstoupdate(3,6);
Eigen::MatrixXd rangosX(6,1);
Eigen::MatrixXd difrangos(6,1);
Eigen::MatrixXd K_k(9,6);
Eigen::MatrixXd I_k(9,9);
Eigen::MatrixXd Inverse_k(6,6);
class EKFUWB //: public DroneModule
 
{
private:
    ros::NodeHandle nh_;
    KalmanFilter KF;
    ros::Subscriber UWB_sub_;
    ros::Publisher kalman_pub;
    vector<float> rangos;
    float noUWB;
    double update_rate;
    double delta_t;
    double var_movi;
    double var_rango;
    double numberoftagtoupdate;
    geometry_msgs::Pose pose;
    int entro;
public:


    void init();
    void run();
    EKFUWB();
    ~EKFUWB();
    void UWBCb(const droneMsgsROS::DecaWaveMsg &msg);


protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();


};

