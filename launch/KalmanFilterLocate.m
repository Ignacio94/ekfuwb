classdef KalmanFilterLocate < handle
    
    properties
        m_update_rate=10;      % Da 5 medidas cada segundo
        m_modelo_mov='PV';    % modelos movimiento 'P' (posicion cte), 'PV' (velo cte), 'PVA' (acel cte)
        m_PosBeacons;         % Posici�n de las balizas (nx3)
        m_num_states=9;       % Son 9 estados fijos pos, velo y acel (vector de 9 elementos)
        m_X_kk;
        m_X_kk1;
        m_P_kk1;
        m_P_kk;
        m_K_k;
        m_Phi;        % Modelo movimiento
        m_var_rango=0.3;  % metros;  % varianza de rango de medida
        m_R;          % Covarianza de medida (s x s): donde "s" es el numero de medidas
        m_var_movi=0.005;  % 0.1 metros
        m_Q;          % covarianza modelo de movimiento
        m_H;
    end
    
    methods
        %===============================================================================
        %------------------CONSTRUCTOR---------------------------
        %===============================================================================
        function obj=KalmanFilterLocate(PosBeacons)
            %PosBeacons: Posici�n de las balizas (nx3)
            
            % inicializo matrices Kalman:
            num_max_rangos=size(obj.m_PosBeacons,1);
            obj.m_PosBeacons=PosBeacons;
            obj.m_X_kk1=zeros(obj.m_num_states,1);
            obj.m_X_kk=zeros(obj.m_num_states,1);
            obj.m_P_kk1=zeros(obj.m_num_states,obj.m_num_states);
            obj.m_P_kk=zeros(obj.m_num_states,obj.m_num_states);
           
            
            % Matrices de covarianza:
            % Q de modelo movimiento:.............................
            % Si Q grande => incertidumbre de la prediccion es alta (P_kkk1 crece) => resulta en estimaciones ruidosas.
            % si Q peque�o => Tengo gran certeza modelo movimiento => filtro mucho (predigo sobre todo),
            %                       pero, si Q tiende a cero, puedo bloquear la estimacion del estado
            obj.m_Q=zeros(obj.m_num_states,obj.m_num_states); % matriz nula
            switch obj.m_modelo_mov
                case 'PVA',
                    obj.m_Q(1:obj.m_num_states,1:obj.m_num_states)=diag(ones(obj.m_num_states,1))*obj.m_var_movi;
                    obj.m_Q(4:6,4:6)=diag(ones(3,1))*obj.m_var_movi*(obj.m_update_rate)^2;
                    obj.m_Q(7:9,7:9)=diag(ones(3,1))*obj.m_var_movi*(obj.m_update_rate)^4;
                case 'PV',
                    obj.m_Q(1:6,1:6)=diag(ones(6,1))*obj.m_var_movi;
                    obj.m_Q(4:6,4:6)=diag(ones(3,1))*obj.m_var_movi*(obj.m_update_rate)^2;
                case 'P',
                    obj.m_Q(1:3,1:3)=diag(ones(3,1))*obj.m_var_movi;
            end
            obj.m_P_kk1=diag(ones(9,1));  % estoy bastante seguro que al principio pos, vel y acel es cero
            % R de modelo de medida:............................
            obj.m_R(:,:)=diag(ones(num_max_rangos,1))*obj.m_var_rango;
            % Si R tiende a cero => no filtra (cree solo en la medida)
            % Si R grande => filtra y suaviza mucho (cree poco en la medida y mas en las prediciones)
            
            % En resumen:
            % Q/R o P/R small => trayectoria suave, pero mal seguimiento
            % Q/R o P/R large => tratectoria ruidosa, pero seguimiento fiel
            
            % ----Jacobiano--- modelo de movimiento (aceleracion cte)
            delta_T=1/obj.m_update_rate;
            switch obj.m_modelo_mov
                case 'PVA',
                    obj.m_Phi(:,:)=zeros(obj.m_num_states,obj.m_num_states);   
                    obj.m_Phi(1:3,1:3)=diag(ones(3,1));
                    obj.m_Phi(4:6,4:6)=diag(ones(3,1));
                    obj.m_Phi(7:9,7:9)=diag(ones(3,1));
                    obj.m_Phi(1:3,4:6)=diag(ones(3,1))*delta_T;
                    obj.m_Phi(4:6,7:9)=diag(ones(3,1))*delta_T;
                    obj.m_Phi(1:3,7:9)=diag(ones(3,1))*0.5*(delta_T)^2;
                case 'PV',
                    obj.m_Phi(:,:)=zeros(obj.m_num_states,obj.m_num_states);
                    obj.m_Phi(1:3,1:3)=diag(ones(3,1));
                    obj.m_Phi(4:6,4:6)=diag(ones(3,1));
                    obj.m_Phi(1:3,4:6)=diag(ones(3,1))*delta_T;
                case 'P',
                    obj.m_Phi(:,:)=zeros(obj.m_num_states,obj.m_num_states);
                    obj.m_Phi(1:3,1:3)=diag(ones(3,1));
            end
        end
        
        %===============================================================================
        %------------------Inicialize-----------------
        %===============================================================================
        function Initialize(obj)
            obj.m_X_kk1=zeros(1,obj.m_num_states);
        end
        
        %===============================================================================
        %------------------Predict---------------------------
        %===============================================================================
        function Predict(obj)
            % ----PREDECIR--- el estado apriori en muestra "k" sin haber hecho la medida a�n
            obj.m_X_kk1=obj.m_Phi*obj.m_X_kk;  % X_k|k-1  "apriori"
            
            % ----- P_k|k-1:  matriz de covarianza de ruido de estimacion apriori
            obj.m_P_kk1=obj.m_Phi*obj.m_P_kk*obj.m_Phi'+ obj.m_Q;
        end
        
        %===============================================================================
        %------------------Update---------------------------
        %===============================================================================
        function Update(obj,rangos,pos_beacons)
            % rangos:      matriz 1xs con rangos ("s" numero de rangos a balizas)
            % pos_beacons  matriz 3xs con posiciones balizas ("s" numero de rangos a balizas)
            
            num_rangos=size(rangos,2);
          %  disp(['Num rangos: ',num2str(num_rangos),' Rango: ',num2str(rangos)]);
            
            if (num_rangos>0 )  % si tengo suficientes peudorangos => actualizo
                obj.m_R=diag(ones(num_rangos,1))*obj.m_var_rango;
                %--------Calcular H------------------------
                % Calculo de los rangos (rango_0) a posicion inicial aproximada del movil (XYZT_0).
                rango_0=sqrt( (obj.m_X_kk1(1)-pos_beacons(1,:)).^2 +...
                    (obj.m_X_kk1(2)-pos_beacons(2,:)).^2 +...
                    (obj.m_X_kk1(3)-pos_beacons(3,:)).^2      )';  %sx1
                
                % Construyo la matriz H
                obj.m_H=[ -(pos_beacons(1,:)'-obj.m_X_kk1(1))./rango_0,...
                    -(pos_beacons(2,:)'-obj.m_X_kk1(2))./rango_0,...
                    -(pos_beacons(3,:)'-obj.m_X_kk1(3))./rango_0,...
                    zeros(num_rangos,6)];  % columnas 4 a 9 ceros          H: sx9
                
                % Diferencia entre rangos medidos y rangos calculados
                delta_rango=rangos' - rango_0;  %sx1 (pseudorango medido menos el aproximado)
                
                % --------K_k: ganancia de Kalman
                obj.m_K_k = obj.m_P_kk1 * obj.m_H' * inv(obj.m_H*obj.m_P_kk1*obj.m_H' + obj.m_R);  % [9xs]
                
                % ---------ACTUALIZAR----- estado: X_k|k = X_k|k-1 + K_k * innova_k
                obj.m_X_kk= obj.m_X_kk1 + obj.m_K_k*delta_rango;  % Nota:  X_kk1(i,:) es la predicci�n del estado
                
                % --------P_k|k:  matriz de covarianza de ruido de estimacion aposteriori  [9x9]
                obj.m_P_kk=(diag(ones(9,1))-obj.m_K_k*obj.m_H)*obj.m_P_kk1; % segun mayor�a de libros
            else
                obj.m_X_kk= obj.m_X_kk1;
                obj.m_P_kk=obj.m_P_kk1;
            end
        end
        %-----------otra funcion--------------
    end  % end-methods
    
end % end-class
