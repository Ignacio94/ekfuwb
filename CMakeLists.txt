cmake_minimum_required(VERSION 2.8.3)
project(EKFUWB)

set(EKFUWB_sources_dir src/sources)
set(EKFUWB_include_dir src/include)
FILE(GLOB_RECURSE EKFUWB_header_files "src/include/*.h")
FILE(GLOB_RECURSE EKFUWB_source_files "src/sources/*.cpp")


find_package(catkin REQUIRED COMPONENTS
  #OpenCV
  #cv_bridge
  #image_transport
  roscpp
  sensor_msgs
  std_msgs  
  droneMsgsROS
  droneModuleROS
  #control_toolbox
  #gazebo_ros 
)


#LINK_DIRECTORIES(/home/ignacio/workspace/ros/aerostack_catkin_ws/devel/lib)
#set(OpenCV_DIR "/home/ignacio/Escritorio/opencv-3.2.0/build")
#find_package(OpenCV 3.2.0 REQUIRED PATHS /home/ignacio/Escritorio/opencv-3.2.0/cmake)
find_package(OpenCV 3.2.0 REQUIRED)
#find_package(VISP REQUIRED)
catkin_package(
  INCLUDE_DIRS ${EKFUWB_include_dir}
#  LIBRARIES ros_sample_image_transport
#  CATKIN_DEPENDS cv_bridge image_transport opencv2 roscpp sensor_msgs std_msgs
#  DEPENDS system_lib
)

include_directories(${EKFUWB_include_dir})
include_directories(
  ${catkin_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME} ${EKFUWB_source_files} ${EKFUWB_header_files})
add_dependencies(${PROJECT_NAME} ${catkin_EXPORTED_TARGETS})
message(STATUS ${OpenCV_INCLUDE_DIRS})
add_executable(EKFUWB_node src/test/EKFUWB_node.cpp)
add_dependencies(EKFUWB_node ${catkin_EXPORTED_TARGETS})
#TARGET_LINK_LIBRARIES(gimbal_process_node cv_bridge)
target_link_libraries(EKFUWB_node  ${PROJECT_NAME} ${OpenCV_LIBRARIES} ${catkin_LIBRARIES})




